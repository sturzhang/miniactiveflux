public class MiniActiveFlux {

    // USER CHANGEABLE PARAMETERS ##########################################################################
    
    protected static double advectionSpeed = 1.0;

    protected static int numberOfTimeSteps = 2;
    
    protected static int nx = 1000;
    protected static double xmin = 0.0;
    protected static double xmax = 1.0;

    protected static double CFL = 0.1;
    protected static double maxTime = 1.0;
    protected static double outputTimeInterval = 0.1;

    protected static final int OUTPUT_DATA  = 1;
    protected static final int OUTPUT_ERROR = 2;
    protected static int myOutput = OUTPUT_DATA;

    protected static boolean printInitial = true;

    protected static boolean doLimiting = true;
    protected static double maxExponentPowerLawLimiting = 50.0;
    
    // PARAMETERS WHICH THE USER SHOULD NOT TOUCH ##########################################################
    
    protected static double[] average = new double[nx];
    protected static double[][] reconCoef = new double[nx][];
    protected static int[] reconType = new int[nx];

    protected static final int PARABOLIC = 0;
    protected static final int POWERLAW = 1;
    
    protected static double[][] leftPointValue = new double[nx][numberOfTimeSteps+1];
    protected static double[] leftFlux = new double[leftPointValue.length];
    
    protected static double dx, xMidpoint;

    // MAIN ###############################################################################################
    
    public static void main(String[] args){
	double time = 0, dt;
	double timeSinceLastOutput = 0;

	createGrid();
	setInitialData();

	if (printInitial){
	    output(time);
	    System.err.println("\nOutput time = " + time);
	}
		
	do {
	    setReconstruction();

	    dt = CFL * dx / Math.abs(advectionSpeed);
	    if (timeSinceLastOutput +dt >= outputTimeInterval){
		dt = outputTimeInterval - timeSinceLastOutput;
	    }
	    
	    setNewPointValues(dt);
	    setFluxes();

	    updateAverages(dt);

	    refreshPointValues();

	    timeSinceLastOutput += dt;
	    time += dt;

	    if (timeSinceLastOutput >= outputTimeInterval){
		output(time);
		System.err.println("\nOutput time = " + time);
		timeSinceLastOutput -= outputTimeInterval;
	    }
	    

	    System.err.print("                          \r");
	    System.err.print("time = " + time);
	} while (time < maxTime);

	output(time);
	System.err.println("\nOutput time = " + time);
		
    }

    // RECONSTRUCTION #################################################################################################################
    
    protected static void setReconstruction(){
	for (int i = 0; i < average.length-1; i++){ setReconstruction(i, leftPointValue[i][0], average[i], leftPointValue[i+1][0]); }
	setReconstruction(average.length-1, leftPointValue[average.length-1][0], average[average.length-1], leftPointValue[0][0]);
    }

    protected static void setReconstruction(int i, double L, double avg, double R){
	if (doLimiting){
	    if (R > L){
		if ((avg <= L) || (avg >= R)){ // extremum unavoidable
		    setReconstructionParabolic(i, L, avg, R);
		} else if ((avg > L + (R - L)/3) && (avg < R - (R - L)/3)){ // no extremum happening
		    setReconstructionParabolic(i, L, avg, R);
		} else { setReconstructionLimited(i, L, avg, R); }    
	    } else if (L > R){
		if ((avg >= L) || (avg <= R)){ // extremum unavoidable
		    setReconstructionParabolic(i, L, avg, R);
		} else if ((avg > (L - R)/3 + R) && (avg < L - (L - R)/3)){ // no extremum happening
		    setReconstructionParabolic(i, L, avg, R);
		} else { setReconstructionLimited(i, L, avg, R); }			
	    } else { // both equal: nothing to do
		setReconstructionParabolic(i, L, avg, R);
	    }
	} else { setReconstructionParabolic(i, L, avg, R); }
    }
    
    protected static void setReconstructionLimited(int i, double L, double avg, double R){
	if (Math.max(Math.max(Math.abs(R), Math.abs(avg)), Math.abs(L)) < 1e-17){
	    // let us not bother with small values
	    setReconstructionParabolic(i, L, avg, R);
	} else {
	    double exponent = (R - avg)/(avg - L);
	    
	    if ((exponent > maxExponentPowerLawLimiting) || (exponent < 1.0/maxExponentPowerLawLimiting)){ // pure machine error issue
		setReconstructionParabolic(i, L, avg, R);
	    } else {
		if (Math.abs(avg - L) < Math.abs(avg - R)) {	
		    setReconstructionPowerLaw(i, L, avg, R, exponent);
		} else {
		    setReconstructionPowerLawInverted(i, L, avg, R, 1.0/exponent);
		}
	    }
	}
    }

    protected static void setReconstructionPowerLaw(int i, double L, double avg, double R, double exponent){
	// L + (R-L)/dx**exponent  (x + dx/2)**exponent
	// exponent = (R - avg)/(avg - L)
	reconCoef[i] = new double[5];
	reconCoef[i][0] = L;
	reconCoef[i][1] = (R - L)*Math.pow(1.0/dx, exponent);
	reconCoef[i][2] = 1.0;
	reconCoef[i][3] = dx/2;
	reconCoef[i][4] = exponent;

	reconType[i] = POWERLAW;
    }

    protected static void setReconstructionPowerLawInverted(int i, double L, double avg, double R, double exponent){
	// R + (L-R)/dx**exponent  (dx/2 - x)**exponent
	// exponent = (avg - L)/(R - avg)
	reconCoef[i] = new double[5];
	reconCoef[i][0] = R;
	reconCoef[i][1] = (L - R)*Math.pow(1.0/dx, exponent);
	reconCoef[i][2] = -1.0;
	reconCoef[i][3] = dx/2;
	reconCoef[i][4] = exponent;

	reconType[i] = POWERLAW;
    }    
    
    protected static void setReconstructionParabolic(int i, double L, double avg, double R){
	reconCoef[i] = new double[3];
	reconCoef[i][0] = (6.0*avg - L - R)/4;
	reconCoef[i][1] = (R - L)/dx;
	reconCoef[i][2] = -3.0*(2.0*avg - L - R)/dx/dx;

	reconType[i] = PARABOLIC;
    }

    // EVOLUTION #####################################################################################################################

    protected static void setNewPointValues(double dt){
	for (int timestep = 1; timestep <= numberOfTimeSteps; timestep++){
	    setNewPointValues(timestep, dt/numberOfTimeSteps*timestep);
	}
    }

    protected static void setNewPointValues(int indexOfTimeStep, double dt){
	int shift;
	double location;
	if (advectionSpeed > 0){
	    shift = -1; location = dx/2 - advectionSpeed * dt;
	} else {
	    shift = 0; location = -dx/2 - advectionSpeed * dt;
	}
	
	for (int i = 0; i < leftPointValue.length; i++){
	    leftPointValue[i][indexOfTimeStep] = evaluateReconstruction(i+shift, location);
	}
    }

    protected static double evaluateReconstruction(int index, double x){
	double res = 0;
	if (index < 0){ index += nx; }
	if (index >= nx){ index -= nx; }

	if (reconType[index] == PARABOLIC){
	    // c0 + c1 x + c2 x**2
	    for (int m = 0; m < reconCoef[index].length; m++){ res += reconCoef[index][m]*Math.pow(x, m); }
	} else if (reconType[index] == POWERLAW){
	    // c0 + c1  (c2 x + c3)**c4
	    res = reconCoef[index][0] + reconCoef[index][1] * Math.pow(reconCoef[index][2] * x + reconCoef[index][3], reconCoef[index][4]);
	} else {
	    System.err.println("ERROR: No such reconstruction!");
	}
	return res;
    }
    
    protected static void setFluxes(){
	for (int i = 0; i < leftPointValue.length; i++){ leftFlux[i] = flux(quadrature(leftPointValue[i])); }
    }

    protected static double flux(double quantity){ return advectionSpeed * quantity; }

    protected static double simpson(double[] value){     return (value[0] + 4.0*value[1] + value[2])/6; }
    protected static double trapezoidal(double[] value){ return (value[0] + value[1])/2; }
    
    protected static double quadrature(double[] value){
	if (       numberOfTimeSteps == 2){ return simpson(value);
	} else if (numberOfTimeSteps == 1){ return trapezoidal(value);
	} else {
	    System.err.println("No quadrature implemented for " + numberOfTimeSteps + " time steps!");
	    return 0;
	}
    }

    protected static void updateAverages(double dt){
	for (int i = 0; i < average.length-1; i++){ average[i] -= dt/dx * (leftFlux[i+1] - leftFlux[i]); }
	average[average.length-1] -= dt/dx * (leftFlux[0] - leftFlux[average.length-1]);
    }

    protected static void refreshPointValues(){
	for (int i = 0; i < leftPointValue.length; i++){ leftPointValue[i][0] = leftPointValue[i][numberOfTimeSteps]; }
    }
    
    // GRID ##########################################################################################################################

    protected static void createGrid(){
	dx = (xmax - xmin) / nx;
	xMidpoint = (xmin + xmax)/2;
    }

    protected static double getXMidpoint(int i){     return xmin + dx/2 + dx * i; }
    protected static double getXLeftBoundary(int i){ return xmin + dx * i;        }

    // INITIALIZATION ###############################################################################################################

    protected static void setInitialData(){
	for (int i = 0; i < leftPointValue.length; i++){
	    leftPointValue[i][0] = initialData(getXLeftBoundary(i));
	}

	for (int i = 0; i < average.length-1; i++){
	    average[i] = simpson(new double[] {leftPointValue[i][0], initialData(getXMidpoint(i)), leftPointValue[i+1][0]});
	}
	average[average.length-1] = simpson(new double[] {leftPointValue[average.length-1][0], initialData(getXMidpoint(average.length-1)), leftPointValue[0][0]});
    }

    protected static double initialData(double x){
	return initialDataRiemannProblem(x);
	//return initialDataGaussian(x);
    }

    protected static double initialDataRiemannProblem(double x){ if (x < xMidpoint){ return 1.0; } else { return 0.1; } }
    protected static double initialDataGaussian(double x){
	x -= xMidpoint;
	double width = (xmax - xmin)/20;
	return Math.exp(-x*x/width/width);
    }
    

    // PRINTING ###################################################################################################################

    protected static void output(double time){
	if (myOutput == OUTPUT_DATA){	
	    for (int i = 0; i < average.length; i++){
		System.out.println("");
		System.out.print(getXLeftBoundary(i) + " ");
		System.out.print(leftPointValue[i][0] + " ");
		
		System.out.println("");
		System.out.print(getXMidpoint(i) + " ");
		System.out.print(average[i] + " ");
	    }
	    
	    System.out.println("");
	    System.out.println("");	
	} else if (myOutput == OUTPUT_ERROR){
	    double err = 0;
	    for (int i = 0; i < leftPointValue.length; i++){
		err += Math.abs(leftPointValue[i][0] - exactSolution(getXLeftBoundary(i), time));
	    }
	    System.out.println(dx + " " + time + " " + (err*dx));
	} else {
	    System.err.println("ERROR: No such output type defined!");
	}
    }

    protected static double exactSolution(double x, double time){
	double xInitial = x - advectionSpeed * time;
	while (xInitial > xmax){ xInitial -= (xmax - xmin); }
	while (xInitial < xmin){ xInitial += (xmax - xmin); }

	return initialData(xInitial);
    }
}
