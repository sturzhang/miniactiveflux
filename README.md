# miniActiveFlux

This is a small code that uses the Active Flux scheme (van Leer 1979, Eymann & Roe 2011) to solve linear advection in 1d.

It is written in java and can be run with

javac *.java; java MiniActiveFlux > data.dat

If you are not familiar with java, just pretend that this is pseudocode. I think the only relevant syntax detail is that 
double[] arr = new double[4];
creates an array with indices arr[0], arr[1], arr[2], arr[3]. 
The code is deliberately written and structured in the style of procedural programming, avoiding object oriented features. This is done in order to help people working with Fortran, C or Matlab. People familiar with object-oriented programming surely will know which things should be moved into separate classes. The code has only some 200 lines, but contains all the elements of a big code.


DISCLAIMER 1: I have tried my best to make sure that this code is free of errors, but I cannot guarantee this.

DISCLAIMER 2: The code, as it is, runs and is short. These are the two requirements for a demonstration code. Real code requires more than that. DON'T USE THIS CODE DIRECTLY FOR YOUR WORK. It violates a number of paradigms of good programming practice. Before extending the code you should tear it apart into different classes or modules. If you use an object-oriented language -- use its features.

